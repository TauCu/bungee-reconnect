package me.taucu.reconnect.api;

import com.google.common.base.Preconditions;
import me.taucu.reconnect.queue.ServerQueue;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Cancellable;
import net.md_5.bungee.api.plugin.Event;

/**
 * Called by a reconnector when it is about to enqueue for a connection attempt.
 */
public class PlayerQueueEvent extends Event implements Cancellable {
    
    private final ServerInfo server;
    private final ServerQueue defaultQueue;
    private final ProxiedPlayer whom;

    private ServerQueue currentQueue;
    private boolean cancelled = false;
    
    public PlayerQueueEvent(ServerInfo server, ServerQueue queue, ProxiedPlayer whom) {
        this.server = server;
        this.defaultQueue = queue;
        this.currentQueue = queue;
        this.whom = whom;
    }
    
    public ServerInfo getServer() {
        return server;
    }
    
    public ProxiedPlayer getPlayer() {
        return whom;
    }

    public boolean isCustomQueue() {
        return currentQueue != defaultQueue;
    }
    
    public ServerQueue getQueue() {
        return currentQueue;
    }

    public void setQueue(ServerQueue queue) {
        Preconditions.checkNotNull(queue);
        this.currentQueue = queue;
    }

    public ServerQueue getDefaultQueue() {
        return defaultQueue;
    }
    
    @Override
    public boolean isCancelled() {
        return cancelled;
    }
    
    @Override
    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }
    
}
