package me.taucu.reconnect.net;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import me.taucu.reconnect.Reconnector;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.BungeeServerInfo;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.netty.HandlerBoss;

public class ReconnectChannelInitializer extends ChannelInitializer<Channel> {

    private final Reconnector connector;
    
    private final ProxyServer bungee;
    private final UserConnection user;
    private final BungeeServerInfo target;
    
    public ReconnectChannelInitializer(Reconnector connector, ProxyServer bungee, UserConnection user, BungeeServerInfo target) {
        this.connector = connector;
        this.bungee = bungee;
        this.user = user;
        this.target = target;
    }
    
    @Override
    protected void initChannel(Channel ch) {
        BungeeCord.getInstance()
                .unsafe()
                .getBackendChannelInitializer()
                .getChannelAcceptor()
                .accept(ch);

        ch.pipeline().get(HandlerBoss.class).setHandler(new ReconnectServerConnector(connector, bungee, user, target));
    }
    
}
