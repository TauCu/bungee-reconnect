package me.taucu.reconnect.queue;

import me.taucu.reconnect.Reconnect;
import net.md_5.bungee.api.ProxyServer;

import java.time.Duration;
import java.time.Instant;

public class QueueTicket {

    private final StandardServerQueue queue;
    private final Reconnect plugin;
    private final Runnable callback;
    private Instant startTime = null;
    private boolean released = false;

    public QueueTicket(StandardServerQueue queue, Runnable callback) {
        this.queue = queue;
        this.plugin = queue.getManager().getPlugin();
        this.callback = callback;
    }

    protected boolean tick() {
        synchronized (this) {
            if (released)
                return true;
            if (startTime == null) {
                startTime = Instant.now();
                ProxyServer.getInstance().getScheduler().runAsync(plugin, callback);
            }
            if (Instant.now().isAfter(startTime.plus(Duration.ofNanos(plugin.getConnectFinalizationNanos())))) {
                release();
                return true;
            }
        }
        return false;
    }

    public void release() {
        synchronized (this) {
            released = true;
        }
    }

    public Instant getStartTime() {
        return startTime;
    }

}
