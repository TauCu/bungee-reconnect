package me.taucu.reconnect.queue;

import me.taucu.reconnect.Reconnector;

public interface ServerQueue {

    QueueTicket enqueue(Reconnector reconnector, Runnable callback);

}
