package me.taucu.reconnect.queue;

import me.taucu.reconnect.Reconnect;
import net.md_5.bungee.api.config.ServerInfo;

import java.util.HashMap;
import java.util.Map;

public class ServerQueueManager {

    private final Map<ServerInfo, StandardServerQueue> serverQueues = new HashMap<>();
    private final Reconnect plugin;

    public ServerQueueManager(Reconnect plugin) {
        this.plugin = plugin;
    }

    public StandardServerQueue getQueueFor(ServerInfo info) {
        synchronized (serverQueues) {
            return serverQueues.computeIfAbsent(info, i -> new StandardServerQueue(this, i));
        }
    }

    public Reconnect getPlugin() {
        return plugin;
    }

}
