package me.taucu.reconnect.queue;

import me.taucu.reconnect.Reconnector;
import net.md_5.bungee.api.config.ServerInfo;

import java.time.Instant;
import java.util.LinkedList;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class StandardServerQueue implements ServerQueue {

    private final LinkedList<QueueTicket> enqueued = new LinkedList<>();
    private final ServerQueueManager manager;
    private final ServerInfo info;

    private ScheduledFuture<?> ticker;
    private Instant lastAttempt;

    public StandardServerQueue(ServerQueueManager manager, ServerInfo info) {
        this.manager = manager;
        this.info = info;
    }

    public QueueTicket enqueue(Reconnector reconnector, Runnable callback) {
        QueueTicket holder = new QueueTicket(this, callback);
        synchronized (enqueued) {
            enqueued.add(holder);
            if (ticker == null || ticker.isCancelled()) {
                manager.getPlugin().getScheduledExecutorService().scheduleAtFixedRate(this::tick, 25, 25, TimeUnit.MILLISECONDS);
            }
        }
        return holder;
    }

    protected void tick() {
        synchronized (enqueued) {
            if (enqueued.isEmpty() && ticker != null && !ticker.isCancelled()) {
                ticker.cancel(true);
            } else {
                Instant now = Instant.now().minusNanos(1);
                QueueTicket ticket;
                while ((ticket = enqueued.peek()) != null) {
                    if (lastAttempt != null && now.isBefore(lastAttempt.plusNanos(manager.getPlugin().getNanosBetweenConnects())))
                        break;
                    if (ticket.getStartTime() == null)
                        lastAttempt = now;
                    if (!ticket.tick())
                        break;
                    enqueued.poll();
                }
            }
        }
    }

    public ServerQueueManager getManager() {
        return manager;
    }

}
