package me.taucu.reconnect.util;

import com.google.common.base.Preconditions;
import io.netty.channel.Channel;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollDomainSocketChannel;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.channel.unix.DomainSocketAddress;

import java.net.SocketAddress;

public class NettyUtil {
    
    public static final boolean EPOLL_AVAILABLE = Epoll.isAvailable();
    
    public static Class<? extends Channel> getChannel(SocketAddress address) {
        if (address instanceof DomainSocketAddress) {
            Preconditions.checkState(EPOLL_AVAILABLE, "Epoll required to have UNIX sockets");
            return EpollDomainSocketChannel.class;
        } else {
            return EPOLL_AVAILABLE ? EpollSocketChannel.class : NioSocketChannel.class;
        }
    }
    
}
